#!/usr/bin/env nextflow

/*
========================================================================================
                                  Civet RNAseq v1.0
        developer: Rupesh Kesharwani; bioinforupesh2009.au@gmail.com
========================================================================================
*/

Channel
  .fromFilePairs( params.reads, size: params.singleEnd ? 1 : 2 )
  .ifEmpty { exit 1, "Cannot find any reads matching: ${params.reads}\nNB: Path needs to be enclosed in quotes!\nNB: Path requires at least one * wildcard!\nIf this is single-end data, please specify --singleEnd on the command line." }
  .set { raw_reads }

/*--------------------------------------------------
  Filter/trim input reads
---------------------------------------------------*/

process qual_stat {

  label "small_task"
  tag "$name"
  publishDir "${params.outdir}/filter", mode: 'copy', pattern: '*_stat'
  cache true

  input:
  set val(name), file(reads) from raw_reads

  output:
  file "*" into filtered_reads
  file "*_stat" into filtered_reads_stat
  set val(name), file("**filtered_trimmed") into (filtered_trimmed, filtered_trimmed_fastqc, filtered_trimmed_grp)

  script:
  """
  module load compsci
  module load it-modules
  module load python/2.7.3 
  filter_trim.py -M 50 ${reads[0]} ${reads[1]}
  """
}

/*--------------------------------------------------
  fastqc of trimmed reads
---------------------------------------------------*/

process fastqc_trimmed {
  label "small_task"
  tag "$name"
  publishDir "${params.outdir}/qc_trimmed", mode: 'copy'
  cache true

  input:
  set val(name), file(reads) from filtered_trimmed_fastqc
  
  output:
  file "*_fastqc.{zip,html}" into qc_trimmedReads
  
  script:
  """
  module load fastqc/0.11.3
  fastqc --noextract -f fastq -q ${reads[0]} ${reads[1]}
  """
}

/*--------------------------------------------------
  Alignment expression
---------------------------------------------------*/

process alignment_expression {
  label "mem_int_with8"
  tag "$name"
  publishDir "${params.outdir}", mode: 'copy', pattern: "*.sorted.bam"
  publishDir "${params.outdir}", mode: 'copy', pattern: "*.sorted.bam.bai"
  publishDir "${params.outdir}", mode: 'copy', pattern: "*.results"
  publishDir "${params.outdir}", mode: 'copy', pattern: "*.stats"
  cache true

  input:
  set val(name), file(reads) from filtered_trimmed

  output:
  file "*" into aligned_expression_bams
  file ("*.genome.sorted.bam") into aligned_genome_bam
  file "*.stats" into aligned_expression_stats
  set val(name), file("*.genes.results"), file("*.isoforms.results") into aligned_expression

  script:
  """
  module purge
  module load compsci
  module load it-modules
  module load samtools/0.1.18
  module load bowtie2/2.2.0
  module load rsem/1.2.12
  
  rsem-calculate-expression -p ${task.cpus} ${params.phredquals} --seed-length ${params.seed_length} --forward-prob ${params.strand_specific} --time --output-genome-bam --bowtie2 --paired-end ${reads[0]} ${reads[1]} ${params.index} $name 2> rsem_aln.stats
  """
}


/*--------------------------------------------------
  Ensembl to Gene conversion
---------------------------------------------------*/

process gene_conversion {
  label "small_task"
  tag "$name"
  publishDir "${params.outdir}", mode: 'copy'
  cache true
  
  input:
  set val(name), file(genefile), file(isoformfile) from aligned_expression
  
  output:
  file "*" into gene_conversion

  script:
  """
  module purge
  module load compsci
  module load it-modules
  module load R
  module load perl/cga

  GeneName_and_Normalization.pl -i1 ${genefile} -i2 ${isoformfile} -a1 ${params.anno}
  """
}

/*--------------------------------------------------
  read group from fastq
---------------------------------------------------*/

process read_group {
  label "tiny_task"
  tag "$name"

  cache true

  input:
  set val(name), file(readone) from filtered_trimmed_grp

  output:
  file "*.txt" into read_grp

  script:
  """
  module purge
  module load compsci
  module load it-modules
  module load python/2.7.3
  
  read_group_from_fastq.py ${readone[0]} ${name}.read_group.txt -p
  """
}

/*--------------------------------------------------
  Aligment Matrix
---------------------------------------------------*/

process aligment_matrix {
  label "small_task"
  tag "$name"
  publishDir "${params.outdir}", mode: 'copy', pattern: '*.txt'
  cache true

  input:
  file(grp) from read_grp
  file(bam) from aligned_genome_bam

  output:
  file("genome_bam_with_read_group.bam") into align_group
  file("genome_bam_with_read_group_reorder.bam") into align_order
  file("aln_metrics.txt") into aln_metrix

  script:
  """
  module purge
  module load compsci
  module load java/1.7.0
  module load bamtools/1.0.2

  java -Xmx8g -jar ${params.addOrReplaceReadGroups} \
  INPUT=${bam} OUTPUT=genome_bam_with_read_group.bam \
  SORT_ORDER=coordinate \$(cat $grp) \
  CREATE_INDEX=true

  java -Xmx8g -jar ${params.reorderSam} \
  INPUT=genome_bam_with_read_group.bam \
  OUTPUT=genome_bam_with_read_group_reorder.bam \
  REFERENCE="${params.ref}" \
  CREATE_INDEX=true

  bamtools stats -insert -in genome_bam_with_read_group_reorder.bam > aln_metrics.txt
  """

}


/*--------------------------------------------------
  Summary
---------------------------------------------------*/

process summary {
  label "small_task"
  publishDir "${params.outdir}", mode: 'copy', pattern: '*.txt'
  tag "$name"

  cache true

  input:
  file(readstat) from filtered_reads_stat
  file(mapstat) from aligned_expression_stats
  file(alnmatrix) from aln_metrix


  output:
  file "*.txt" into summarystats

  script:
  """
  module load compsci
  module load it-modules
  module load perl/cga

  summary_QC_metrics_without_xenome.pl ${readstat} ${mapstat} ${alnmatrix} > summary_stats.txt
  """
}
