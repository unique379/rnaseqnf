# RNASeqNF
Version v1.0

Analysis: Non-strand-specific (0.5 default) protocol analysis/quanitifcation of RNAseq using RSEM and bowtie2. Change accordingly into config of nextflow.
Set to 1 for a strand-specific protocol where all (upstream) reads are derived from the forward strand, 0 for a strand-specific protocol where all (upstream) 
read are derived from the reverse strand, or 0.5 for a non-strand-specific protocol.

Steps are included: 
1. bad quality reads filtering: bad quality script using custom script (tools/bin/filter_trim.py)
2. fastqc reports: trimmed reads from filter directory
3. Mapping and Quantification: using RSEM and bowtie2
4. conversion of gene and isoform results into gene symbol: using custom script
5. alignment statistics: using genome bam

#How to run:

git clone https://unique379@bitbucket.org/unique379/rnaseqnf.git

#change to your working directory
cd Myworking_dir/
mkdir MyResults

#run
nextflow run rnaseqnf/civet_RNASeqNextFlow_v1.0.nf -c rnaseqnf/nextflow.config --reads "rnaseqnf/testdata/one*{R1,R2}.fastq" --outdir MyResults -resume

Note: The Pipleline assumes nextflow is installed and set to your PATH (bashrc/bash_profile).


Availability and requirements:

Note: This pipeline is required pbs cluster to run. Generic version of this pipeline is coming soon...
Please change within config file a full path of pre-built bowtie2-rsem based indices, fasta, custom annotation and location of picard tools (AddOrReplaceReadGroups.jar and ReorderSam.jar) .

PBS schedule
python/2.7.3
fastqc/0.11.3
samtools/0.1.18
bowtie2/2.2.0
rsem/1.2.12
perl/cga
picard/1.95
human genome hg38
pre-build indices of RSEM and bowtie2
annotation file with ENSEMBL gene/isoform and standard gene/ucsc symbols. 
Example:
ENST00000373020	ENSG00000000003	TSPAN6	uc004ega.1
ENST00000494424	ENSG00000000003	TSPAN6	uc004ega.1
ENST00000496771	ENSG00000000003	TSPAN6	uc004ega.1
ENST00000612152	ENSG00000000003	TSPAN6	uc004ega.1
ENST00000614008	ENSG00000000003	TSPAN6	uc004ega.1

For any assistance and debugging:
Please contact: bioinforupesh2009.au@gmail.com; rupesh.kesharwani@jax.org
Last updated date: 24/12/2019 ##^^^^Thank you for using our RNaseq Quantification pipeline^^##
